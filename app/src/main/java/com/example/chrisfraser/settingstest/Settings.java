package com.example.chrisfraser.settingstest;

/**
 * Created by Chris Fraser on 11/15/2016.
 */
public class Settings {
    public String foo;
    public String bar;
    public int baz;

    public Settings(String foo, String bar, int baz) {
        this.foo = foo;
        this.bar = bar;
        this.baz = baz;
    }
}
