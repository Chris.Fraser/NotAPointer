package com.example.chrisfraser.settingstest;

/**
 * Created by Chris Fraser on 11/15/2016.
 */

public class ServiceLocator {

    private Settings settings;
    private SettingsUser settingsUser;

    public void start() {
        settings = new Settings("Hello", "World", 1);

        settingsUser = new SettingsUser(settings);

        settings = new Settings("Hello", "David", 2);

        settingsUser.test(settings);

        settings = new Settings("Hello", "Hi", 3);

        settingsUser.setSettings(settings);
    }
}
