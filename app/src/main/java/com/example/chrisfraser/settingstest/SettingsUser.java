package com.example.chrisfraser.settingstest;

import android.util.Log;

/**
 * Created by Chris Fraser on 11/15/2016.
 */
public class SettingsUser {
    private Settings settings;
    private String tag = "SettingsUser";

    public SettingsUser(Settings settings) {
        this.settings = settings;
    }

    public void test(Settings settings) {
        if(this.settings.baz == settings.baz){
            Log.d(tag,"Pointer");
        } else{
            Log.d(tag,"Not Pointer");
        }
    }

    public void setSettings(Settings settings) {
        if(this.settings.baz == settings.baz){
            Log.d(tag,"Pointer");
        } else {
            Log.d(tag,"Still Not Pointer");
        }
    }
}
